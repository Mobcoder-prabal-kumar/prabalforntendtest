import React from 'react'
import "./design.css"
import headerImage from '../asset/img/logo-white.png'
import countryLogo from '../asset/img/united-kingdom.svg'
import learningImage from '../asset/img/learning-exp.png'
import hatImage from '../asset/img/Icon.svg'
import bookImage from '../asset/img/Layer_10.svg'
import readingBook from '../asset/img/reading-book.svg'
import presentaion from '../asset/img/presentation.svg'
import miniLogo from '../asset/img/logo-mini.png'
import mainSectionImage from '../asset/img/banner-img.png'
export default function Design() {
  return (
    <div>
      <section className='header-section'>
        <div className='container'>
          <div className='header'>
            <img src={headerImage} alt="no-image" />
            <ul>
              <li>Home</li>
              <li>About Us</li>
              <li>Courses</li>
              <li>Contact</li>
            </ul>
            <div className='right-header'>
              <img src={countryLogo} alt="no-image" />
              <p>Login</p>
              <button>GET STARTED</button>
            </div>
          </div>
        </div>
      </section>
      <section className='main-section'>
        <div className='container'>
          <div className='main-section-detail'>
            <div className='mainsection-rignt'>
              <img src={miniLogo} alt="" />
              <h2>
                Let's build skills with IFA & learn without Limits...
              </h2>
              <p>Take your learning to the next level</p>
            </div>
            <img src={mainSectionImage} alt="no-image" />
          </div>
        </div>

      </section>
      <section className='learning-section'>
        <div className='container'>
          <div className='learning'>
            <img src={learningImage} alt="no-image" />
            <div className='learning-right-section'>
              <h1>Learn Anywhere, Anytime</h1>
              <h2>Positive Learning Experiences At Your Fingertips</h2>
              <p>Access digital educational content directly on your mobile device and interact with a learning bot through any one of your preferred social messaging platforms. Come collaborate with peers and educators from around the world, and exchange knowledge and ideas as life-long learners.</p>
              <button>REGISTER AS LEARNER</button>
            </div>
          </div>
        </div>
      </section>
      <section className='card-section'>
        <div className='container'>
          <div className='card-section-detail'>
            <h2>Empowering Minds, Impacting Communities</h2>
            <p>We’re becoming the premiere eLearning hub for remote workers and communities across the globe, offering localized content that reflects their various cultures, languages, and learning styles.</p>
          </div>
        </div>
        <div className='card-Details'>
          <div className='card'>
            <img src={hatImage} alt="no-image" />
            <h2>742+</h2>
            <p>Active Learners</p>
          </div>
          <div className='card'>
            <img src={bookImage} alt="no-image" />
            <h2>65+</h2>
            <p>Educators</p>
          </div>
          <div className='card'>
            <img src={readingBook} alt="no-image" />
            <h2>x</h2>
            <p>Courses Available</p>
          </div>
          <div className='card'>
            <img src={presentaion} alt="no-image" />
            <h2>x</h2>
            <p>Communities Reached</p>
          </div>
        </div>

      </section>

    </div>
  )
}
